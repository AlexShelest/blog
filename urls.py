from django.conf.urls import url
from django.contrib.auth import views as auth_views
from blog import views
urlpatterns = [
                         url(r'^$', views.home, name='home'),
                         url(r'^about/$', views.about, name='about'),
                         url(r'^article/(?P<article_id>[0-9]+)/$', views.show_article, name='article'),
                         url(r'^login/$', auth_views.login, {'template_name': 'blog/login.html'}, name='login'),
                         url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
]
